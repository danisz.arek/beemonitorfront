import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class BeehiveService {

    getBeehiveById(beehiveId) {

        return axios.get(API_URL + 'beehive/' + beehiveId,
            {headers: authHeader()}
        );
    }

    addNewBeehive(beehiveobject) {
        return axios.post(API_URL + 'beehives/', JSON.stringify(beehiveobject),
            {headers: authHeader()}
        );
    }

    editSpecificBeehive(beehiveobject, beehiveId) {
        return axios.put(API_URL + 'beehive/' + beehiveId, JSON.stringify(beehiveobject),
            {headers: authHeader()}
        );
    }

    deleteSpecificBeehive(beehiveId) {
        return axios.delete(API_URL + 'beehive/' + beehiveId,
            {headers: authHeader()}
        );
    }
}

export default new BeehiveService();
