import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class InspectionService {

    addNewInspection(inspectionObject) {
        return axios.post(API_URL + 'inspections', JSON.stringify(inspectionObject),
            {headers: authHeader()}
        );
    }

    getInspectionsByBeehiveId(beehiveId) {
        return axios.get(API_URL + 'inspections/' + beehiveId,
            {headers: authHeader()}
        );
    }

    editInspection(inspectionObject, inspectionId) {
        return axios.put(API_URL + 'inspection/' + inspectionId, JSON.stringify(inspectionObject),
            {headers: authHeader()}
        );
    }

    getSpecificInspectionById(inspectionId) {
        return axios.get(API_URL + 'oneinspection/' + inspectionId,
            {headers: authHeader()}
        );
    }

    deleteSpecificInspection(inspectionId) {
        return axios.delete(API_URL + 'inspection/' + inspectionId,
            {headers: authHeader()}
        );
    }
}

export default new InspectionService();
