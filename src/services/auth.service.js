import axios from 'axios';

const API_URL = 'http://83.230.14.77/bee/';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'user/login', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'user', {
      username: user.username,
      password: user.password
    });
  }
}

export default new AuthService();
