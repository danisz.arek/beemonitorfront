import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class SensorService {

    addNewSensor(sensorObject) {
        return axios.post(API_URL + 'beesensors', JSON.stringify(sensorObject),
            {headers: authHeader()}
        );
    }

    getSensorsByBeehiveId(beehiveId) {
        return axios.get(API_URL + 'beesensors/' + beehiveId,
            {headers: authHeader()}
        );
    }

    editSensor(sensorObject, sensorId) {
        return axios.put(API_URL + 'beesensor/' + sensorId, JSON.stringify(sensorObject),
            {headers: authHeader()}
        );
    }

    getSpecificSensorById(sensorId) {
        return axios.get(API_URL + 'beesensor/' + sensorId,
            {headers: authHeader()}
        );
    }

    deleteSpecificSensor(sensorId) {
        return axios.delete(API_URL + 'beesensor/' + sensorId,
            {headers: authHeader()}
        );
    }

    getSensorDataBySensorId(sensorId) {
        return axios.get(API_URL + 'beesensordatas/' + sensorId,
            {headers: authHeader()}
        );
    }

    getActualSensorData(sensorId) {
        return axios.get(API_URL + 'actualbeesensordata/' + sensorId,
            {headers: authHeader()}
        );
    }

    getMonthlySensorData(sensorId) {
        return axios.get(API_URL + 'thismonthbeesensordata/' + sensorId,
            {headers: authHeader()}
        );
    }

    getWeeklySensorData(sensorId) {
        return axios.get(API_URL + 'thisweekbeesensordata/' + sensorId,
            {headers: authHeader()}
        );
    }

    getYearlySensorData(sensorId) {
        return axios.get(API_URL + 'thisyearbeesensordata/' + sensorId,
            {headers: authHeader()}
        );
    }
}

export default new SensorService();
