import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class BeegardenService {

    getAllBeegardensFromUser(userId) {

        return axios.get(API_URL + 'beegardens/' + userId,
            {headers: authHeader()}
        );
    }

    getSpecificBeegardenById(beegardenId) {
        return axios.get(API_URL + 'beegarden/' + beegardenId,
            {headers: authHeader()}
        );
    }

    getAllBeehivesFromSpecificBeegarden(beegardenIds) {

        return axios.get(API_URL + 'beehivesfrombeegardens/' + beegardenIds,
            {headers: authHeader()});
    }

    addNewBeegarden(beegardenObject) {
        return axios.post(API_URL + 'beegardens/', JSON.stringify(beegardenObject),
            {headers: authHeader()}
        );
    }

    editBeegarden(beegardenObject, beegardenId) {
        return axios.put(API_URL + 'beegarden/' + beegardenId, JSON.stringify(beegardenObject),
            {headers: authHeader()}
        );
    }

    deleteSpecificBeegarden(beegardenId) {
        return axios.delete(API_URL + 'beegarden/' + beegardenId,
            {headers: authHeader()}
        );
    }
}

export default new BeegardenService();
