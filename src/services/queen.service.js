import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class QueenService {

    addNewQueen(queenObject) {
        return axios.post(API_URL + 'beequeens', JSON.stringify(queenObject),
            {headers: authHeader()}
        );
    }

    getQueensByBeehiveId(beehiveId) {
        return axios.get(API_URL + 'beequeens/' + beehiveId,
            {headers: authHeader()}
        );
    }

    editQueen(queenObject, queenId) {
        return axios.put(API_URL + 'beequeen/' + queenId, JSON.stringify(queenObject),
            {headers: authHeader()}
        );
    }

    getSpecificQueenById(queenId) {
        return axios.get(API_URL + 'onebeequeen/' + queenId,
            {headers: authHeader()}
        );
    }

    deleteSpecificQueen(queenId) {
        return axios.delete(API_URL + 'beequeen/' + queenId,
            {headers: authHeader()}
        );
    }
}

export default new QueenService();
