import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class UserService {

    getUserBoard(userId) {
        return axios.get(API_URL + 'userdetails/' + userId,
            {headers: authHeader()}
        );
    }

    getAdminBoard() {
        return axios.get(API_URL + 'userdetails', {headers: authHeader()});
    }

    getBeehiveBoard(ajdi) {
        // eslint-disable-next-line no-console
        console.log("Get beehives for specific beegarden with id: " + ajdi)
        return axios.get(API_URL + 'beehives/' + ajdi, {headers: authHeader()});
    }

    deleteBeegarden(ajdi) {
        return axios.put(API_URL + 'beegardens/' + ajdi,
            {"name": "piiiiii", "userId": 2, "broken": false}
            , {headers: authHeader()});
    }

    addNewUserDetails(userDetailsObject) {
        return axios.post(API_URL + 'userdetails', JSON.stringify(userDetailsObject),
            {headers: authHeader()}
        );
    }

    editUserDetails(userDetailsObject, userId) {
        return axios.put(API_URL + 'userdetails/' + userId, JSON.stringify(userDetailsObject),
            {headers: authHeader()}
        );
    }

    deleteAccount(userId) {
        return axios.delete(API_URL + 'user/' + userId,
            {headers: authHeader()}
        );
    }

    getAmountOfBeegardens(userId) {
        return axios.get(API_URL + 'countuserbeegardens/' + userId, {headers: authHeader()});
    }


    getAmountOfBeehives(userId) {
        return axios.get(API_URL + 'countbeehives/' + userId,
            {headers: authHeader()}
        );
    }

    getAmountOfSensors(userId) {
        return axios.get(API_URL + 'countsensors/' + userId,
            {headers: authHeader()}
        );
    }
}

export default new UserService();
