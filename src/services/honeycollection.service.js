import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class HoneycollectionService {

    addNewHoneyCollection(honeyCollectionObject) {
        return axios.post(API_URL + 'honeycollections', JSON.stringify(honeyCollectionObject),
            {headers: authHeader()}
        );
    }

    getHoneyCollectionsByBeehiveId(beehiveId) {
        return axios.get(API_URL + 'honeycollections/' + beehiveId,
            {headers: authHeader()}
        );
    }

    editHoneyCollection(honeyCollectionObject, honeyCollectionId) {
        return axios.put(API_URL + 'honeycollection/' + honeyCollectionId, JSON.stringify(honeyCollectionObject),
            {headers: authHeader()}
        );
    }

    getSpecificHoneyCollectionById(honeyCollectionId) {
        return axios.get(API_URL + 'onehoneycollection/' + honeyCollectionId,
            {headers: authHeader()}
        );
    }

    deleteSpecificHoneyCollection(honeyCollectionId) {
        return axios.delete(API_URL + 'honeycollection/' + honeyCollectionId,
            {headers: authHeader()}
        );
    }

    getAmountOfHoneyByBeehiveId(beehiveId) {
        return axios.get(API_URL + 'amountofhoney/' + beehiveId,
            {headers: authHeader()}
        );
    }
}

export default new HoneycollectionService();
