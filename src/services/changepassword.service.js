import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://83.230.14.77/bee/';

class ChangePasswordService {

    changePassword(changePasswordObject) {
        return axios.post(API_URL + 'me/changepassword', JSON.stringify(changePasswordObject),
            {headers: authHeader()}
        );
    }

    tryLogin(loginObject) {
        return axios.post(API_URL + 'user/login', JSON.stringify(loginObject),
            {headers: authHeader()}
        );
    }
}

export default new ChangePasswordService();
