import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/User/Login.vue';
import Register from './views/User/Register.vue';

Vue.use(Router);

export const router = new Router({
    mode: 'hash',
    duplicateNavigationPolicy: 'reload',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/register',
            component: Register
        },
        {
            path: '/profile',
            name: 'profile',
            // lazy-loaded
            component: () => import('./views/User/Profile.vue')
        },
        {
            path: '/admin',
            name: 'admin',
            // lazy-loaded
            component: () => import('./views/Boards/BoardAdmin.vue')
        },
        {
            path: '/user',
            name: 'user',
            // lazy-loaded
            component: () => import('./views/Boards/BoardUser.vue')
        },
        {
            path: '/boardbeegardens',
            name: 'BoardBeegardens',
            // lazy-loaded
            component: () => import('./views/Boards/BoardBeegardens.vue')
        },
        {
            path: '/boardbeehives',
            name: 'BoardBeehives',
            // lazy-loaded
            component: () => import('./views/Boards/BoardBeehives.vue')
        },
        {
            path: '/boardbeehives',
            name: 'BoardBeehives',
            // lazy-loaded
            component: () => import('./views/Boards/BoardBeehives.vue')
        },
        {
            path: '/beehive',
            name: 'Beehive',
            // lazy-loaded
            component: () => import('./views/Beehive/Beehive.vue')
        },
        {
            path: '/addbeegarden',
            name: 'AddBeegarden',
            // lazy-loaded
            component: () => import('./views/Beegarden/AddBeegarden.vue')
        },
        {
            path: '/editbeegarden',
            name: 'EditBeegarden',
            // lazy-loaded
            component: () => import('./views/Beegarden/EditBeegarden.vue')
        },
        {
            path: '/addBeehive',
            name: 'AddBeehive',
            // lazy-loaded
            component: () => import('./views/Beehive/AddBeehive.vue')
        },
        {
            path: '/editbeehive',
            name: 'EditBeehive',
            // lazy-loaded
            component: () => import('./views/Beehive/EditBeehive.vue')
        },
        {
            path: '/addhoneycollection',
            name: 'AddHoneyCollection',
            // lazy-loaded
            component: () => import('./views/HoneyCollection/AddHoneyCollection.vue')
        },
        {
            path: '/edithoneycollection',
            name: 'EditHoneyCollection',
            // lazy-loaded
            component: () => import('./views/HoneyCollection/EditHoneyCollection.vue')
        },
        {
            path: '/addinspection',
            name: 'AddInspection',
            // lazy-loaded
            component: () => import('./views/Inspection/AddInspection.vue')
        },
        {
            path: '/editinspection',
            name: 'EditInspection',
            // lazy-loaded
            component: () => import('./views/Inspection/EditInspection.vue')
        },
        {
            path: '/addqueen',
            name: 'AddQueen',
            // lazy-loaded
            component: () => import('./views/Queen/AddQueen.vue')
        },
        {
            path: '/editqueen',
            name: 'EditQueen',
            // lazy-loaded
            component: () => import('./views/Queen/EditQueen.vue')
        },
        {
            path: '/addsensor',
            name: 'AddSensor',
            // lazy-loaded
            component: () => import('./views/Sensor/AddSensor.vue')
        },
        {
            path: '/editsensor',
            name: 'EditSensor',
            // lazy-loaded
            component: () => import('./views/Sensor/EditSensor.vue')
        },
        {
            path: '/adduserdetails',
            name: 'AddUserDetails',
            // lazy-loaded
            component: () => import('./views/UserDetails/AddUserDetails.vue')
        },
        {
            path: '/edituserdetails',
            name: 'EditUserDetails',
            // lazy-loaded
            component: () => import('./views/UserDetails/EditUserDetails.vue')
        },
        {
            path: '/changepassword',
            name: 'ChangePassword',
            // lazy-loaded
            component: () => import('./views/User/ChangePassword.vue')
        },
        {
            path: '/sensor',
            name: 'Sensor',
            // lazy-loaded
            component: () => import('./views/Sensor/Sensor.vue')
        },
        {
            path: '/honeycollections',
            name: 'Collections',
            // lazy-loaded
            component: () => import('./views/HoneyCollection/Collections.vue')
        },
        {
            path: '/inspections',
            name: 'Inspections',
            // lazy-loaded
            component: () => import('./views/Inspection/Inspections.vue')
        },
        {
            path: '/beequeens',
            name: 'BeeQueens',
            // lazy-loaded
            component: () => import('./views/Queen/BeeQueens.vue')
        },
        {
            path: '/sensorarchive',
            name: 'SensorArchiveData',
            // lazy-loaded
            component: () => import('./views/SensorData/SensorArchiveData.vue')
        },
    ]
});
