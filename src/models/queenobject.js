export default class queenobject {
    constructor(queenNo, colour, fertile, variety, dateOfApplication, beehiveId) {
        this.queenNo = queenNo;
        this.colour = colour;
        this.fertile = fertile;
        this.variety = variety;
        this.dateOfApplication = dateOfApplication;
        this.beehiveId = beehiveId;
    }
}
