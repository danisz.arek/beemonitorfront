export default class beegardenobject {
    constructor(beegardenName, location, type, userId) {
        this.beegardenName = beegardenName;
        this.location = location;
        this.type = type;
        this.userId = userId;
    }
}
