export default class BeehiveObject {
    constructor (barrier, beegardenId, beehiveName, bottom, dateOfCreation, force, feeder, framesAmount,
                grubs, heater, honey, inletWidth, isolator, lackOfFood, poolenTrapper,
                propolis, propolisSinker, queenCage, queenCell, swarmMood, temperament, trunksAmount,
                type, workFramesAmount) {


        this.barrier=barrier;
        this.beegardenId=beegardenId;
        this.beehiveName=beehiveName;
        this.bottom=bottom;
        this.dateOfCreation=dateOfCreation;
        this.force=force;
        this.feeder = feeder;
        this.framesAmount=framesAmount;
        this.grubs=grubs;
        this.heater=heater;
        this.honey=honey
        this.inletWidth=inletWidth;
        this.isolator=isolator;
        this.lackOfFood=lackOfFood;
        this.poolenTrapper=poolenTrapper;
        this.propolis=propolis;
        this.propolisSinker=propolisSinker;
        this.queenCage=queenCage;
        this.queenCell=queenCell;
        this.swarmMood=swarmMood;
        this.temperament=temperament;
        this.trunksAmount=trunksAmount;
        this.type=type;
        this.workFramesAmount=workFramesAmount;

    }
}
