export default class userdetailsobject {
    constructor(beekeperName, beekeperSurname, email, postCode, city, road, roadNr, user) {
        this.beekeperName = beekeperName;
        this.beekeperSurname = beekeperSurname;
        this.email = email;
        this.postCode = postCode;
        this.city = city;
        this.road = road;
        this.roadNr = roadNr;
        this.user = user;
    }
}
