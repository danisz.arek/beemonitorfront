export default class userdetailsobject {
    constructor(id, beekeperName, beekeperSurname, email, postCode, city, road, roadNr) {
        this.id = id;
        this.beekeperName = beekeperName;
        this.beekeperSurname = beekeperSurname;
        this.email = email;
        this.postCode = postCode;
        this.city = city;
        this.road = road;
        this.roadNr = roadNr;
    }
}
