export default class inspectionobject {
    constructor(inspectionDate, feeding, queen, treatment, notes, beehiveId) {
        this.inspectionDate = inspectionDate;
        this.feeding = feeding;
        this.queen = queen;
        this.treatment = treatment;
        this.notes = notes;
        this.beehiveId = beehiveId;
    }
}
