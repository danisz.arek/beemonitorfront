export default class sensordataobject {
    constructor(readDate, temperature, humidity, latitude, logitude, beeSensorId) {
        this.readDate = readDate;
        this.temperature = temperature;
        this.humidity = humidity;
        this.latitude = latitude;
        this.logitude = logitude;
        this.beeSensorId = beeSensorId

    }
}
