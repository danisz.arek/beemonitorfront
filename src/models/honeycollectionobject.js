export default class honeycollectionobject {
    constructor(collectionDate, honeyType, amount, beehiveId) {
        this.collectionDate = collectionDate;
        this.honeyType = honeyType;
        this.amount = amount;
        this.beehiveId = beehiveId;
    }
}
