# Vue.js beemonitoring system

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Build docker
```
docker build -t test .
```

### Run docker
```
docker run -d --publish 8081:80 test
```

### AWS stuff
```
aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin {{ID}}.dkr.ecr.eu-central-1.amazonaws.com
docker tag test:latest {{ID}}.dkr.ecr.eu-central-1.amazonaws.com/test:latest
docker push {{ID}}.dkr.ecr.eu-central-1.amazonaws.com/test:latest
```
